import React, { Component } from "react";
import {
  Toolbar,
  ToolbarItem,
  Button,
  Accordion,
  AccordionItem,
  TextInput,
  Form
} from "carbon-components-react";
import {
  attackEnemies,
  receiveDamage,
  getCount,
  playerList,
  getId,
  updateHealth,
  changeName
} from "./Api";
import Progress from "react-progressbar";
import "./App.css";

class App extends Component {
  state = {
    death: 0,
    healthPoints: 100,
    damage: 10,
    count: 0,
    playerList: [],
    damageDealt: 0,
    score: 0,
    cooldown: false,
    selectedPlayer: "",
    name: ""
  };

  handleAttack = () => {
    const params = {
      damage: this.state.damage,
      target: this.state.selectedPlayer,
      attacker: getId()
    };

    attackEnemies(params);
    const damageDealt = this.state.damageDealt + this.state.damage;
    this.setState({ damageDealt });
    this.updateScore(this.state.damage);
    this.handleReload();
  };

  handleGetAttacked = damage => {
    this.setState(prevState => ({
      healthPoints: prevState.healthPoints - damage
    }));
    if (this.state.healthPoints <= 0) {
      this.handleRestart();
      this.updateScore(-100);
    }
    updateHealth(this.state.healthPoints);
  };

  handleRestart = () => {
    this.setState(prevState => ({
      healthPoints: 100,
      death: prevState.death + 1
    }));
  };

  handleGetUserCount = count => {
    this.setState({ count });
  };

  handleGetPlayerList = playerList => {
    this.setState({ playerList });
  };

  handleReload = () => {
    this.setState({ cooldown: true });
    setTimeout(() => this.setState({ cooldown: false }), 1500);
  };

  handleSelectPlayer = selectedPlayer => {
    this.setState({ selectedPlayer });
  };

  handleChangeUsername = e => {
    e.preventDefault();
    const name = document.getElementById("username-input").value;
    this.setState({ name });
    document.getElementById("username-input").value = "";
    //Update name on websocket
    changeName(name);
  };

  updateScore = addedValue => {
    const score = this.state.score + addedValue;
    this.setState({ score });
  };

  componentDidMount() {
    receiveDamage(
      damage => this.handleGetAttacked(damage),
      this.state.healthPoints
    );
    getCount(count => this.handleGetUserCount(count));
    playerList(list => this.handleGetPlayerList(list));
  }

  render() {
    return (
      <div className="app">
        <header className="navbar">
          <Toolbar>
            <ToolbarItem>
              <h1>Welcome to Char d'Assaut</h1>
            </ToolbarItem>
          </Toolbar>
        </header>
        <div className="main-container">
          <div className="actions">
            <h2>Select your ennemy and fire at will!</h2>
            <br />
            <Progress
              className="health-bar"
              completed={this.state.healthPoints}
            />
            <br />
            <Button
              onClick={this.handleAttack}
              disabled={
                this.state.cooldown || this.state.selectedPlayer.length === 0
              }
            >
              {this.state.cooldown
                ? "Reloading..."
                : this.state.selectedPlayer.length > 0
                  ? "Attack"
                  : "Target a player"}
            </Button>
          </div>
          <br />
          <br />
          <Accordion className="accordion">
            <AccordionItem title="Infos">
              <ul>
                <li>
                  <h2>- Players online: {this.state.count}</h2>
                </li>
                <li>
                  <h2>- Your user ID: {getId()}</h2>
                </li>
                {this.state.name.length > 0 && (
                  <li>
                    <h2>- Your username: {this.state.name}</h2>
                  </li>
                )}
                <li>
                  <h2>- Health points: {this.state.healthPoints}</h2>
                </li>
                <li>
                  <h2>- Damage dealt: {this.state.damageDealt}</h2>
                </li>
                <li>
                  <h2>- Death count: {this.state.death}</h2>
                </li>
                <li>
                  <h2>- Score: {this.state.score}</h2>
                </li>
              </ul>
              <br />
              <Form
                className="change-username"
                onSubmit={e => this.handleChangeUsername(e)}
              >
                <TextInput
                  placeholder="Your username"
                  id="username-input"
                  labelText=""
                />
                <Button type="submit">Confirm</Button>
              </Form>
            </AccordionItem>
            <AccordionItem title="Player list">
              <ul>
                {this.state.playerList.map(player => (
                  <li style={{ marginBottom: 20 }} key={player.id}>
                    {" "}
                    <Progress
                      className="health-bar"
                      completed={player.healthPoints}
                    />
                    <br />
                    <Button
                      onClick={() => this.handleSelectPlayer(player.id)}
                      disabled={this.state.selectedPlayer === player.id}
                    >
                      {player.name.length > 0 ? player.name : player.id}
                    </Button>
                  </li>
                ))}
              </ul>
            </AccordionItem>
          </Accordion>
        </div>
      </div>
    );
  }
}

export default App;
