import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import "carbon-components/css/carbon-components.min.css";
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
