import openSocket from "socket.io-client";
const API_URL = "https://chardassaut.herokuapp.com/"; //For prod
// const API_URL = "localhost:5000"; //For dev
const socket = openSocket(API_URL);

const receiveDamage = callback => {
  socket.on("damage", damage => {
    callback(damage);
  });
};

const updateHealth = newHealth => {
  socket.emit("gotDamaged", newHealth);
};

const attackEnemies = params => {
  socket.emit("attack", params);
};

const getCount = callback => {
  socket.on("getCount", total => callback(total));
};

const playerList = callback => {
  socket.on("playerList", list =>
    callback(list.filter(player => player.id !== socket.id))
  );
};

const changeName = newName => {
  socket.emit("changeName", newName);
};

const getId = () => socket.id;

export {
  receiveDamage,
  attackEnemies,
  getCount,
  playerList,
  getId,
  updateHealth,
  changeName
};
